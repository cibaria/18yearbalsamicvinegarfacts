
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>18 Year Old Balsamic Vinegars - Get the Facts</title>

    <link rel="stylesheet" type="text/css" href="css/styles.css" />
    <!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="css/ie7.css" />
    <![endif]-->
    <!--[if IE]>
    <link rel="stylesheet" type="text/css" href="css/ie.css" />
    <![endif]-->

    <script type="text/javascript" src="js/jquery-1.5.2.min.js"></script>
    <script type="text/javascript" src="js/plugins.js"></script>
    <script type="text/javascript" src="js/scripts.js"></script>

</head>
<body>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-44118324-2', 'auto');
    ga('send', 'pageview');

</script>
<div id="nojs-background">
    <img src="images/backgrounds/background1.jpg" alt="Is Your Balsamic Vinegar 18 years old" /><!-- Non-JavaScript
    background -->
</div>
<!--Start Outside-->
<div class="outside"><a name="top"></a>
    <!-- Begin Header -->
    <div id="header-outer" class="clearfix">
        <div id="header-inner">
            <!--Logo-->
            <div class="logo"></div>
            <!--End Logo-->
            <!-- Nav -->
            <div id="nav-wrap">
                <div id="nav-wrap-inner">
                    <!--Horizontal Menu-->
                    <ul id="horz-nav-ul" class="sf-menu">
                        <h1 style="text-align:center;">18 Year Balsamic Vinegar Facts</h1>
                        <h2 style="text-align:center"><a href="facts/index.html">Balsamic Vinegar Facts</a></h2>

                    </ul>
                    <!--End Horizontal Menu-->
                    <!--Search-->
                    <div class="search-pop-up">


                    </div>
                </div>
            </div>
        </div>
        <!-- End Nav -->
    </div>
</div>
<!-- End Header -->
<!-- Begin HOME PAGE -->
<div class="content-outer clearfix">
    <div class="content-inner">

    </div>
</div>
<!-- End HOME PAGE -->
<!-- Begin Footer -->


<div id="footer">
    <div class="footer-inside clearfix">
        <!--Footers Left Column-->
        <div class="foot-left-col">
            <?php include('includes/src/rss.php'); ?>


        </div>
        <!--End Footers Left Column-->
        <!--Footers Right Column-->
        <div class="foot-right-col">

            <div style="margin-top:5px;"></div>
            <a href="http://olivaamore.com/" title="Oliva Amore">Oliva Amore</a> | <a
                href="http://wholesalegourmetoliveoil.com/" title="Wholesale Gourmet Olive Oil">Wholesale Gourmet
                Olive Oil</a>
            <!--Hidden pop out boxes - Social icons-->
            <div class="footer-pop-out-box social-pop-out-box">
                <div class="clearfix">
                    <div class="one-icon-wrap"><a href="#"><img src="images/social-icons/facebook.png" width="32" height="32" alt="Facebook" /></a></div>
                    <div class="one-icon-wrap"><a href="#"><img src="images/social-icons/delicious.png" width="32" height="32" alt="Delicious" /></a></div>
                    <div class="one-icon-wrap"><a href="#"><img src="images/social-icons/flickr.png" width="32" height="32" alt="Flickr" /></a></div>
                    <div class="one-icon-wrap"><a href="#"><img src="images/social-icons/rss.png" width="32" height="32" alt="RSS" /></a></div>
                    <div class="one-icon-wrap"><a href="#"><img src="images/social-icons/twitter.png" width="32" height="32" alt="Twitter" /></a></div>
                </div>
            </div>

        </div>
    </div>
    <!-- End Footer -->
</div>
<!--End Outside-->
<script type="text/javascript">Cufon.now();</script>
</body>
</html>